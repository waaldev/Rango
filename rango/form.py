from django import forms
from .models import Category, Page, UserProfile
from django.contrib.auth.models import User


class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=Category.max_length, help_text='نام موضوع را وارد کنید.', label='نام')
    views = forms.IntegerField(widget=forms.HiddenInput, initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput, initial=0)
    slug = forms.SlugField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Category
        fields = ('name', )


class PageForm(forms.ModelForm):
    title = forms.CharField(max_length=128, help_text='عنوان صفحه را وارد کنید.', label='عنوان')
    url = forms.URLField(max_length=200, help_text='آدرس صفحه را وارد کنید.', label='آدرس')
    views = forms.IntegerField(widget=forms.HiddenInput, initial=0)

    class Meta:
        model = Page
        exclude = ('Category',)

        def clean(self):
            cleaned_data = self.cleaned_data
            url = cleaned_data.get('url')
            if url and not url.startswith('http://'):
                url = 'http://' + url
                cleaned_data['url'] = url
            return cleaned_data


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class UserProfileForm(forms.ModelForm):
    website = forms.URLField(required=False, label='وب‌سایت شخصی')
    picture = forms.ImageField(required=False, label='آواتار')

    class Meta:
        model = UserProfile
        exclude = ('user', )






